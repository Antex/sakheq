<?php
	require_once('twitter/twitteroauth/twitteroauth.php');
	require_once('gtranslate.php');
	
	define('CONSUMER_KEY', '');
	define('CONSUMER_SECRET', '');
	
	define('ACCESS_TOKEN', "");
	define('ACCESS_TOKEN_SECRET', "");

	function generateAuthLink(){
		$oauth = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
	 
		$request = $oauth->getRequestToken();
	 
		$requestToken       = $request['oauth_token'];
		$requestTokenSecret = $request['oauth_token_secret'];
	 
		$requestStr = 'request_token=' . $requestToken . "\n";
		$requestStr .= 'request_token_secret=' . $requestTokenSecret . "\n";
	 
		file_put_contents('request.txt', $requestStr);
	 
		// Создаем ссылку
		$registerURL = $oauth->getAuthorizeURL($request);
		// Показываем ссылку
		echo $registerURL . PHP_EOL;
	}

	function generateOauthToken($requestToken, $requestTokenSecret, $pin){
	 
		$oauth = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $requestToken, $requestTokenSecret);
	 
		// Создаем коды доступа по PIN коду
		$request = $oauth->getAccessToken($pin);
		$accessToken       = $request['oauth_token'];
		$accessTokenSecret = $request['oauth_token_secret'];
	 
		$requestStr = 'accessToken=' . $accessToken . "\n";
		$requestStr .= 'accessTokenSecret=' . $accessTokenSecret . "\n";
	 
		file_put_contents('access.txt', $requestStr);
	 
		echo "ok";
	}

	
	function distance($lat1, $lon1, $lat2, $lon2, $unit) { 
	  $theta = $lon1 - $lon2; 
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
	  $dist = acos($dist); 
	  $dist = rad2deg($dist); 
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
		return ($miles * 1.609344); 
	  } else if ($unit == "N") {
		  return ($miles * 0.8684);
		} else {
			return $miles;
		  }
	}
	
	function short($url){
		$url = urlencode($url);
		return file_get_contents("http://tlk.rs/a?url=$url");
	}

	function sendToTwitter($message, $lat = 0, $lon = 0){
		print $message . PHP_EOL;
		$oauth = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
	 
		// Проверяем от кого будут идти сообщение
		$credentials = $oauth->get("account/verify_credentials");
	
		if($lat && $lon){
		    return $oauth->post('statuses/update', array(
		        "status" => $message,
		        "lat" => $lat,
		        "long" => $lon,
			"display_coordinates" => "true",
			"geo_enabled" => "true"
		    ));
		} else {
		    return $oauth->post('statuses/update', array(
		        "status" => $message
		    ));
		
		}

	}

	$my_coord = array(46.9581, 142.7337);
	
	$try = 1;
	$quakes = '';
	
	while($try <= 5 && trim($quakes) == ''){
		print "fetching..." . PHP_EOL;
		$try++;
		$quakes = file_get_contents("http://earthquake.usgs.gov/earthquakes/catalogs/eqs1day-M2.5.txt");
		sleep(5);
	}
	
	$last = file_get_contents("/tmp/quake.last");
	
	$quakes = explode(PHP_EOL, $quakes);
	unset($quakes[0]);
	$quakes = array_reverse($quakes);
	
	foreach($quakes as $quake){
		if($quake){
			$quake = str_getcsv($quake);
			$udate = strtotime($quake[3]);
			$date = date("d-m-Y H:i:s", ($udate));
			$lat = $quake[4];
			$lon = $quake[5];
			$mg = $quake[6];
			$depth = $quake[7];
			$region = $quake[9];
			$distance = (int) distance($lat, $lon, $my_coord[0], $my_coord[1], 'K');
			
			if($udate > $last && $distance <= 1000){
				$url = "http://maps.google.com/maps?z=12&q=loc:$lat+$lon";
				$surl = short($url);
				$gt = new GoogleTranslater();
				$tregion = $gt->translateText($region, 'en', 'ru');
				
				if(!$tregion)
				    $tregion = $region;
				    
				$message = "{$date} M{$mg}, {$tregion}, глуб. {$depth}км., расст. {$distance}км. #eq #sakh $surl";
				
				if(sendToTwitter($message, $lat, $lon)){
					file_put_contents("/tmp/quake.last", $udate);
				}
			} else {
				print "skiped $udate $distance" . PHP_EOL;
			}
		}
	}
	
	